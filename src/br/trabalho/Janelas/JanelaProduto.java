
package br.trabalho.Janelas;

import br.trabalho.repositorio.ProdutoBD;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author camelobboy
 */
public class JanelaProduto extends JFrame{
    
    
    private JButton inserirButton;
    private JButton apagarButton;
    private JButton atualizarButton;
    private JButton selecionarButton;
    private JButton listarButton;
    
    private JLabel  nomeProdutoJLabel;
    private JTextField nomeProdutoJTextField;
    
    private JLabel quantidadePodutoJLabel;
    private JTextField quantidadeProdutoJTextField;
    
    private JLabel idProdutoJLabel;
    private JTextField idProdudoJTextField;
    
    private JPanel cadastroJPanel;
    private JPanel botoesJPanel;
    
    private Container container;
    
    private ProdutoBD pb = new ProdutoBD();
    
    public JanelaProduto(){
        super("cadastro produto");
        
        nomeProdutoJLabel =new JLabel("produto");
        nomeProdutoJTextField = new JTextField(30);
        
        quantidadePodutoJLabel = new JLabel("quantidade");
        quantidadeProdutoJTextField =new JTextField(30);
        
        idProdutoJLabel = new JLabel("id produto");
        idProdudoJTextField = new JTextField(30);
        
        cadastroJPanel = new JPanel(new GridLayout(4,1));
        cadastroJPanel.add(nomeProdutoJLabel);
        cadastroJPanel.add(nomeProdutoJTextField);
        cadastroJPanel.add(quantidadePodutoJLabel);
        cadastroJPanel.add(quantidadeProdutoJTextField);
        cadastroJPanel.add(idProdutoJLabel);
        cadastroJPanel.add(idProdudoJTextField);
        
        inserirButton = new JButton("inserir");
       inserirButton.addActionListener((ActionEvent arg0) -> {
                    // TODO Auto-generated method stub
                    try {
                        pb.inserir(nomeProdutoJTextField.getText(), quantidadeProdutoJTextField.getText(),idProdudoJTextField.getText());
                        
                        JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
                    }catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                    // TODO Auto-generated catch block
                });
        selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				nomeProdutoJTextField.setText("");
				nomeProdutoJTextField.setText(pb.listar(idProdudoJTextField.getText()));			
			}
			
		});	
                apagarButton = new JButton("Apagar");
		apagarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pb.apagar(idProdudoJTextField.getText());
					idProdudoJTextField.setText("");
					nomeProdutoJTextField.setText("");
					JOptionPane.showMessageDialog(null,"Registro excluído com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Você errou!!"+e.getMessage());
				}
			}
			
		});	

                atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener((ActionEvent arg0) -> {
                    try {
                        // TODO Auto-generated method stub
                        pb.atualizar(nomeProdutoJTextField.getText(), quantidadeProdutoJTextField.getText(), idProdudoJTextField.getText());
                       JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");
                    } catch (SQLException e) {
                        
                        JOptionPane.showMessageDialog(null, "Você errou!!"+e.getMessage());
                    }
                                       
                });	
                listarButton = new JButton("Listagem");
		listarButton.addActionListener((ActionEvent arg0) -> {
                    JanelaListagem jl=new JanelaListagem();
                });
                botoesJPanel = new JPanel(new GridLayout(1,5));
		botoesJPanel.setPreferredSize(new Dimension(100,50));
		botoesJPanel.add(inserirButton);
		botoesJPanel.add(selecionarButton);
		botoesJPanel.add(apagarButton);
		botoesJPanel.add(atualizarButton);
		botoesJPanel.add(listarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout(1,1));
		container.add(cadastroJPanel, BorderLayout.CENTER);
		container.add(botoesJPanel, BorderLayout.SOUTH);
		
		setSize(500,250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		JanelaProduto jp=new JanelaProduto();
	}
}
        
        
    
    
            
    
