
package br.trabalho.Janelas;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.trabalho.repositorio.ItemBD;
import java.util.logging.Level;
import java.util.logging.Logger;



public class JanelaItem extends JFrame {

	
	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton selecionarButton;
	private JButton listarButton;
	
        private  JLabel quantidadeLabel;
        private  JTextField quantidadeTextField;
        
	private JLabel idItemLabel;
	private JTextField idItemTextField;

     
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	//Conexao com o banco de dados;
	private ItemBD pB = new ItemBD();
	
	public JanelaItem() {
		super(" item!!");
                
		
                quantidadeLabel = new JLabel("quantidade");
		quantidadeTextField = new JTextField(30);
                
               
                
		idItemLabel = new JLabel("id_item");
		idItemTextField= new JTextField(10);
                
            
		cadastroPanel = new JPanel(new GridLayout(4,1));
                                
		cadastroPanel.add( quantidadeLabel);
		cadastroPanel.add(quantidadeTextField);
                
                               
                cadastroPanel.add(idItemLabel);
                cadastroPanel.add(idItemTextField);
                				
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener((ActionEvent arg0) -> {
                    // TODO Auto-generated method stub
                    try {
                        pB.inserir(quantidadeTextField.getText(), idItemTextField.getText());
                        //pB.inserir(quantidadeTextField.getText(), idItemTextField.getText());
                        JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
                    }catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                    // TODO Auto-generated catch block
                });
		
		selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener((ActionEvent arg0) -> {
                    quantidadeTextField.setText("");
                    quantidadeTextField.setText(pB.listar(idItemTextField.getText()));
                });		
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener((ActionEvent arg0) -> {
                    try {
                        // TODO Auto-generated method stub
                        pB.apagar(idItemTextField.getText());
                    } catch (SQLException ex) {
                        Logger.getLogger(JanelaItem.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    quantidadeTextField.setText("");
                    idItemTextField.setText("");
                    
                    JOptionPane.showMessageDialog(null,"Registro excluído com sucesso!!");
                });		
		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener((ActionEvent arg0) -> {
                    try {
                        // TODO Auto-generated method stub
                        pB.atualizar(quantidadeTextField.getText(), idItemTextField.getText());
                       
                    } catch (SQLException ex) {
                        Logger.getLogger(JanelaItem.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");
                });		
		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener((ActionEvent arg0) -> {
                    JanelaListagem jl=new JanelaListagem();
                });
		
		botoesPanel = new JPanel(new GridLayout(1,5));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(selecionarButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(500,250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		JanelaItem itens =new JanelaItem();
	}
	
}
