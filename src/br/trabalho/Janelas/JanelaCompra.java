
package br.trabalho.Janelas;

import br.trabalho.repositorio.CompraBD;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 *
 * @author camelobboy
 */
public class JanelaCompra extends JFrame{
   
   private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton selecionarButton;
	private JButton listarButton;
	
        private  JLabel quantidadeCompraLabel;
        private  JTextField quantidadeCompraTextField;
        
	private JLabel idCompraLabel;
	private JTextField idCompraTextField;
        
        

     
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	//Conexao com o banco de dados;
	private CompraBD pB = new CompraBD();
	
	public JanelaCompra() {
		super(" minhas compras!!");
                
		
                quantidadeCompraLabel = new JLabel("quantidade de compra");
		quantidadeCompraTextField = new JTextField(30);
                
               
                
		idCompraLabel = new JLabel("id_compra");
		idCompraTextField= new JTextField(10);
                				
            
		cadastroPanel = new JPanel(new GridLayout(4,1));
                                
		cadastroPanel.add( quantidadeCompraLabel);
		cadastroPanel.add(quantidadeCompraTextField);
                
                               
                cadastroPanel.add(idCompraLabel);
                cadastroPanel.add(idCompraTextField);
                
                				
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pB.inserir(idCompraTextField.getText(), quantidadeCompraTextField.getText());
					JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Você errou!!"+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});

		
		selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener((ActionEvent arg0) -> {
                    quantidadeCompraLabel.setText("");
                    quantidadeCompraTextField.setText(pB.listar(idCompraTextField.getText()));
                });		
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener((ActionEvent arg0) -> {
                    try {
                        // TODO Auto-generated method stub
                        pB.apagar(idCompraTextField.getText());
                    } catch (SQLException ex) {
                        Logger.getLogger(JanelaCompra.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    idCompraTextField.setText("");
                    quantidadeCompraTextField.setText("");
                    JOptionPane.showMessageDialog(null,"Registro excluído com sucesso!!");
                });	

		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener((ActionEvent arg0) -> {
                    // TODO Auto-generated method stub
                    try {
                        pB.atualizar(idCompraTextField.getText(), quantidadeCompraTextField.getText());
                        JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, "Você errou!!"+e.getMessage());
                    }
                });		


		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener((ActionEvent arg0) -> {
                    JanelaListagem jl=new JanelaListagem();
                });
		
		botoesPanel = new JPanel(new GridLayout(1,5));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(selecionarButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(500,250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		JanelaCompra  compras=new JanelaCompra();
	}
	
}
