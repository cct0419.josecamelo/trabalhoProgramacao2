/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author camelobboy
 */
public class ItemDAO {
    public int cadastrar(String quantidadeItem,String idItem) throws SQLException {
            
            
           
                  Connection con = ConexaoBancoDeDados.getConnection();
		
		PreparedStatement pStatement = con.prepareStatement("INSERT INTO item VALUES(?,?)");
		
		
		pStatement.setString(1, quantidadeItem);
		pStatement.setInt(2, Integer.parseInt(idItem));
		
		
		
		int resultado = pStatement.executeUpdate();
		
			pStatement.close();
		
		
		con.close();
		
		
		return resultado;
	}
	
}

    

