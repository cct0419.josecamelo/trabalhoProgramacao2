
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author camelobboy
 */
public class CompraDAO {
   
	public int cadastrar(String quantidadeCompra,String idCompra) throws SQLException {
            
            
           
                  Connection con = ConexaoBancoDeDados.getConnection();
		
		PreparedStatement pStatement = con.prepareStatement("INSERT INTO compra VALUES(?,?)");
		
		
		pStatement.setString(1, quantidadeCompra);
		pStatement.setInt(2, Integer.parseInt(idCompra));
		
		
		
		int resultado = pStatement.executeUpdate();
		
			pStatement.close();
		
		
		con.close();
		
		
		return resultado;
	}
	
}
