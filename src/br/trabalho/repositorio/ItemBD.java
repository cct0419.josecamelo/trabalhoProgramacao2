
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author camelobboy
 * iditem
 * qtd
 */
public class ItemBD {
     public void inserir(String quantidadeItem,String idItem) throws Exception {
         try (Connection conn = ConexaoBancoDeDados.getConnection()) {
             if(conn !=null){
                 System.out.println("conexao feita ");
             }
             try (PreparedStatement stmt = conn.prepareStatement("insert into item values(?,?)")) {
                 stmt.setDouble(2, Double.parseDouble(quantidadeItem));
                 stmt.setInt(1, Integer.parseInt(idItem));
                 
                 if (quantidadeItem.equals("")) {
                     throw new Exception("quantidade não pode ser vazio!!");
                 } else {
                     stmt.executeUpdate();
                 }
             }
         }
	}
	
	public void apagar(String idItem) throws SQLException {
         try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("delete from item where idItem=?")) {
             stmt.setInt(1, Integer.parseInt(idItem));
             stmt.executeUpdate();
         }		
	}
	
	public void atualizar(String idItem,String quantidadeItem)throws SQLException {
         try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("update item set idItem=? where quantidadeItem=?")) {
             stmt.setInt(1, Integer.parseInt(idItem));
             stmt.setDouble(2, Double.parseDouble(quantidadeItem));
             
             stmt.executeUpdate();
             stmt.close();
	     conn.close();

             
         }
	}
	
	public List<String> listarTodosItens(){
		List<String> itenscompra = new ArrayList<String>();
		try {
			Connection conn = ConexaoBancoDeDados.getConnection();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from item");
			while (rs.next()) {
				itenscompra.add(rs.getString(2));
			}
		} catch (SQLException e) {
		}
		return itenscompra;
	}
	
	public String listar(String idItem) {
		String itenscompra="";
		
		try {
                    try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("select iditem from Item where quantidadeItem=?")) {
                        stmt.setInt(1, Integer.parseInt(idItem));
                        ResultSet rs = stmt.executeQuery();
                        if (rs.next()) {
                            itenscompra = rs.getString(1);
                        }
                        if (itenscompra.equals("")){
                            throw new Exception("O nome do item está em branco!!");
                        }
                    }
		} catch (SQLException e) {
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return itenscompra;
	}
	
}

    

