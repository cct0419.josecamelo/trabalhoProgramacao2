
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



/**
 *
 * @author camelobboy
 */

   public class ConexaoBancoDeDados {
	
	
	private static String user = "mysql";
	
	private static String password = "mysql";
	
	private static String location = "jdbc:mysql://localhost:3306/programacao";
	
	private static Connection con = null;
	
	
	public static Connection getConnection() throws SQLException {
		
		if (con == null) {
			
			con = DriverManager.getConnection(location, user, password);
		}else {
			
			con.close();
			con = DriverManager.getConnection(location,user,password);
		}
		
		return con;
	}

}
