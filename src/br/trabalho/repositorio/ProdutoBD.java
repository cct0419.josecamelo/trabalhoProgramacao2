
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author camelobboy
 */
public class ProdutoBD {
    
     public void inserir(String nomeProduto,String quantidadeProduto, String idProduto) throws Exception {
        try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("insert into produto values(?,?,?)")) {
            stmt.setString(1, nomeProduto);
            stmt.setString(2, quantidadeProduto);
            stmt.setInt(3, Integer.parseInt(idProduto));
            
            if (nomeProduto.equals("")) {
                throw new Exception("Nome produto não pode ser vazio!!");
            } else {
                stmt.executeUpdate();
                stmt.close();
		conn.close();

            }
        }
	}
	
	public void apagar(String idProduto) throws SQLException {
        try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("delete from produto where idProduto=?")) {
            
            stmt.setInt(3,Integer.parseInt(idProduto));
            stmt.executeUpdate();
            stmt.close();
	    conn.close();

        }		
	}
	
	public void atualizar(String nomeProduto, String quantidadeProduto,String idProduto) throws SQLException {
        try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("update produto set nomeProduto=? where quantidadeProduto=? where idProduto=?")) {
            stmt.setString(1, nomeProduto);
            stmt.setString(2, quantidadeProduto);
            stmt.setInt(3,Integer.parseInt(idProduto));
            stmt.executeUpdate();
            stmt.close();
	    conn.close();
        }
	}
	
	public List<String> listarTodos(){
		List<String> nomeProduto = new ArrayList<String>();
		try {
			Connection conn = ConexaoBancoDeDados.getConnection();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from cliente");
			while (rs.next()) {
				 nomeProduto.add(rs.getString(2));
			}
		} catch (SQLException e) {
		}
		return  nomeProduto;
	}
	
	public String listar(String idProduto) {
		String  nomeProduto="";
		
		try {
                    try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("select nome from cliente where nome=?")) {
                        stmt.setInt(1,Integer.parseInt(idProduto));
                        ResultSet rs = stmt.executeQuery();
                        if (rs.next()) {
                             nomeProduto = rs.getString(1);
                        }
                        if ( nomeProduto.equals("")){
                            throw new Exception("O nome do produto está em branco!!");
                        }
                    }
		} catch (SQLException e) {
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return  nomeProduto;
	}


}
    

